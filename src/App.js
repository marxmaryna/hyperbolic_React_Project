import React, {useState} from 'react';
import './App.scss';
import {InputGroup,FormControl,Button} from "react-bootstrap";
import {useStoreState, useStoreActions} from 'easy-peasy';
import MovieModal from './MovieModal'

function App() {

  const [movies,setMovies] = useState([]);
  const [display, setDisplay] = useState('list');
  const [view, setView] = useState('movies');
  const [search, setSearch] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [focussedMovie, setFocussedMovie] = useState([]);

  let url = 'https://movie-database-imdb-alternative.p.rapidapi.com/?s=' + search +'&r=json&page=1';
  let options = {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "movie-database-imdb-alternative.p.rapidapi.com",
      "x-rapidapi-key": "a89588bc38mshae4b804cc0fe764p11dcaejsne8beeee7f864"
    }
  }

  let headerHeight = '100vh';
  movies.length > 0 ? headerHeight = '100px' : headerHeight = '100vh'

  function getMovies(){
    fetch(url, options).then(async (response) => {
      const data = await response.json();
      setMovies(data.Search)
    }).catch(err => {
      console.error(err);
    });

    setView('movies')
  }

  function getMovieDetails(movie){
    let IDurl = 'https://movie-database-imdb-alternative.p.rapidapi.com/?r=json&i=' + movie;
    setTimeout(() => {
      fetch(IDurl, options).then(async (response) => {
        const data = await response.json();
        setFocussedMovie(data)
      }).catch(err => {
        console.error(err);
      });
    }, 1000);


    setTimeout(() => {
      setShowModal(true)
    }, 1500);

  }

  function Favorites() {
    const favorites = useStoreState((state) => state.favorites.movies);
    const removeFavorite = useStoreActions((actions) => actions.favorites.removeMovie);
    return (
      <div>
        <Button variant="primary-solid" onClick={() => setView('movies')} className="mb-3">Back to Results</Button>
        {favorites.map((item, i) => {
          return(
            display === 'list' ?
            <div className="list-view" key={i}>
                <div key={i} className="list-grid">
                  <div>
                    {item.Poster !== 'N/A' ? 
                      <img src={item.Poster} alt={item.Title + "-cover"} width={200}/>
                      : <img src='placeholder.png' alt={item.Title + "-cover"} width={200}/>
                    }
                  </div> 
                  <div><span className="text-sm">Title:</span><br/>{item.Title}</div> 
                  <div><span className="text-sm">Type:</span><br/>{item.Type}</div> 
                  <div><span className="text-sm">Year:</span><br/>{item.Year}</div> 
                  <Button variant="danger" onClick={() => removeFavorite(item)} className="mt-3 mb-2">Remove from Favourites</Button>
                </div>
              </div>
            :
              <div className="grid-view" key={i}>
                <div key={i} className="grid">
                  <div className="image-container">
                    {item.Poster !== 'N/A' ? 
                      <img src={item.Poster} alt={item.Title + "-cover"} width={'100%'}/>
                      : <img src='placeholder.png' alt={item.Title + "-cover"} width={'100%'}/>
                    }
                  </div> 
                  <div><span className="text-sm">Title:</span><br/>{item.Title}</div> 
                  <div><span className="text-sm">Type:</span><br/>{item.Type}</div> 
                  <div><span className="text-sm">Year:</span><br/>{item.Year}</div> 
                  <Button variant="danger" onClick={() => removeFavorite(item)} className="mt-2 mb-2">Remove from Favourites</Button>
                </div>
              </div>
          )
        })}
      </div>
    )
  }

  return (
    <div className="App">
        <div className="App-header" style={{height: headerHeight}}>
          <InputGroup className="mb-3">
            <FormControl
              placeholder="Search IMDb"
              aria-label="Search IMDb"
              onChange={(e) => setSearch(e.target.value)}
            />
              <Button variant="secondary" onClick={() => getMovies()}><img src="search.png" alt="search-icon" width={10}/></Button>
              <Button variant='primary' onClick={() => setView('favorites')}><img src="heart.png" alt="search-icon" width={10} style={{marginRight: '10px'}}/>VIEW FAVOURITES</Button>
          </InputGroup>
        </div>
        {movies.length > 0 && view === 'movies' ?
          <div className="App-body">
            <div>
              <Button variant={display === 'list' ? 'primary-solid' : 'primary-outlined'} onClick={() => setDisplay('list')}>List View</Button>
              <Button variant={display === 'grid' ? 'primary-solid' : 'primary-outlined'} onClick={() => setDisplay('grid')}>Grid View</Button>
            </div>
            <h2>Results</h2>
            {display === 'list' ?
              <div className="list-view">
                  {movies.map((item, i) => {
                    return(
                      <div key={i} className="list-grid" onClick={() => (getMovieDetails(item.imdbID))}>
                        <div>
                          {item.Poster !== 'N/A' ? 
                            <img src={item.Poster} alt={item.Title + "-cover"} width={200}/>
                            : <img src='placeholder.png' alt={item.Title + "-cover"} width={200}/>
                          }
                        </div> 
                        <div><span className="text-sm">Title:</span><br/>{item.Title}</div> 
                        <div><span className="text-sm">Type:</span><br/>{item.Type}</div> 
                        <div><span className="text-sm">Year:</span><br/>{item.Year}</div> 
                      </div>
                    )
                  })}
              </div>
            :
            <div className="grid-view">
              {movies.map((item, i) => {
                return(
                  <div key={i} className="grid" onClick={() => (getMovieDetails(item.imdbID))}>
                    <div className="image-container">
                      {item.Poster !== 'N/A' ? 
                        <img src={item.Poster} alt={item.Title + "-cover"} width={'100%'}/>
                        : <img src='placeholder.png' alt={item.Title + "-cover"} width={'100%'}/>
                      }
                    </div> 
                    <div><span className="text-sm">Title:</span><br/>{item.Title}</div> 
                    <div><span className="text-sm">Type:</span><br/>{item.Type}</div> 
                    <div><span className="text-sm">Year:</span><br/>{item.Year}</div> 
                  </div>
                )
              })}
            </div>
            }
          </div>
        :view === 'favorites' ?
            <div className="App-body">
              <div>
                <Button variant={display === 'list' ? 'primary-solid' : 'primary-outlined'} onClick={() => setDisplay('list')}>List View</Button>
                <Button variant={display === 'grid' ? 'primary-solid' : 'primary-outlined'} onClick={() => setDisplay('grid')}>Grid View</Button>
              </div>
              <h2>Favourites</h2>
              <Favorites />
            </div>
        :null}
        <MovieModal showModal={showModal} closeModal={() => setShowModal(false)} data={focussedMovie}/>
      </div>
  );
}

export default App;
