import React, {useState} from "react";
import {Container, Row, Col, Modal, Button} from "react-bootstrap";
import {useStoreActions, useStoreState} from 'easy-peasy'


export function MovieModal(props){
    const favorites = useStoreState((state) => state.favorites.movies);
    const [buttonFunction, setButtonFunction] = useState('');

    function checkButtonFunction(){
        favorites.map((item, i) => {
            if(item.Title === props.data.Title){
                setButtonFunction('remove')
            } else {
                setButtonFunction('add')
            }
        })
    }
    
    function AddToFavorites(){
        const saveFavorite = useStoreActions((actions) => actions.favorites.saveMovie);
        return(<AddToFavorite onSave={saveFavorite}/>)
    }

    function AddToFavorite() {
        const saveFavorite = useStoreActions((actions) => actions.favorites.addMovie);
        const removeFavorite = useStoreActions((actions) => actions.favorites.removeMovie);
        checkButtonFunction()
        return (
            buttonFunction === 'add' || buttonFunction === '' ?
                <Button variant="success" onClick={() => {saveFavorite(props.data); setButtonFunction('remove')}}>Add to Favourites</Button>
            : <Button variant="danger" onClick={() => {removeFavorite(props.data); setButtonFunction('add')}}>Remove from Favourites</Button>
        );
    }
    let genre;
    if(props.data.length > 0){
        genre = props.data.Genre.split(',')
        return genre
    }

    return (
        <Modal className="movie-modal" show={props.showModal} onHide={props.closeModal} size="lg">
            <Modal.Header>
                <Modal.Title>
                    <Row>
                        <Col sm={12}>
                            {props.data.Title} <br/>
                        </Col>
                        <Col sm={10}>
                            <p className="text-sm mb-0 mt-2">{props.data.Year} | {props.data.Rated} | {props.data.Runtime}</p>
                            {genre ?
                                genre.map((item, i) => {
                                    return(
                                        <Button key={i} disabled variant="primary-outlined">{item}</Button>
                                    )
                                })
                            :null}
                            <p className="text-sm mb-0 mt-2">{props.data.Awards}</p>
                        </Col>
                        <Col sm={2} className="text-right">
                            <p className="mb-0"><span className="text-sm">IMDb RATING</span><br/><img src="star.png" width={20} alt="star-icon"/> {props.data.imdbRating} <span className="text-sm">/10</span></p>
                        </Col>
                    </Row>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col><img src={props.data.Poster} alt={props.data.Title + '-cover'} width={'100%'}/></Col>
                        <Col>
                            <p>{props.data.Plot}</p>
                            <hr/>
                            <p><b>Directors:</b> {props.data.Director}</p>
                            <hr/>
                            <p><b>Writers:</b> {props.data.Writer}</p>
                            <hr/>
                            <p><b>Actors:</b> {props.data.Actors}</p>
                            <hr/>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <AddToFavorites/>
                <Button variant="secondary" onClick={props.closeModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default MovieModal;
