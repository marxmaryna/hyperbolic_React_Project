import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {StoreProvider, createStore, action, thunk, persist} from 'easy-peasy';
import axios from 'axios';

const favoriteList = createStore(persist({
  favorites: {
    movies: [],
    addMovie: action((state, payload) => {
      state.movies.push(payload)
    }),
    saveMovie: thunk(async (actions, payload) => {
      const { movieData }  = await axios.post('/favorites', payload);
      actions.addMovie(movieData)
    }),
    removeMovie: action((state, payload) => {
      state.movies = state.movies.filter((movie) => movie.Title !== payload.Title)
    }),
  }
}))

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider store={favoriteList}>
      <App />
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
